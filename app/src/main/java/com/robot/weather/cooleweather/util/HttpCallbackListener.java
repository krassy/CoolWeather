package com.robot.weather.cooleweather.util;

/**
 * 作者：krassy on 2018/7/16 0016 15:30
 * 邮箱：chenbin@rl160.com
 */
public interface HttpCallbackListener {
    void onFinish(String response);
    void onError(Exception e);
}
