package com.robot.weather.cooleweather.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.robot.weather.cooleweather.service.AutoUpdateService;

/**
 * 作者：krassy on 2018/7/16 0016 16:22
 * 邮箱：chenbin@rl160.com
 */
public class AutoUpdateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, AutoUpdateService.class);
        context.startService(i);
    }
}
